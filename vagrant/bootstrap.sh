#!/usr/bin/env bash

# Custom configuration
WEB_NAME="disroot.lan"
WEB_ROOT="/var/www/"
WWW_USER="www-data"
YAML="/var/www/disroot.lan/user/config/system.yaml"

# Provisioning actions

# Avoid Postfix installation interactive screens by preconfiguring this information
sudo debconf-set-selections <<< "postfix postfix/main_mailer_type select No configuration"
sudo debconf-set-selections <<< "postfix postfix/mailname string ${WEB_NAME}"

# Add sury repository to sources.list for PHP7.4
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y install ca-certificates apt-transport-https
echo "deb https://packages.sury.org/php/ stretch main" | sudo tee -a /etc/apt/sources.list.d/php.list

# Sury Key for PHP7.4
wget -q https://packages.sury.org/php/apt.gpg -O- | sudo apt-key add -
sudo apt-get -y update

echo "Installing nginx..."
sudo apt-get install -y nginx-full

echo "Installing composer..."
sudo apt-get install -y composer

echo "Installing php7..."
sudo apt-get install -y php7.4-zip php7.4-cli php7.4-curl php7.4-gd php7.4-mbstring php7.4-xml php7.4-fpm

# Create the Nginx config files and restart webserver
sudo rsync -cr /vagrant/provision/etc/nginx/sites-available /etc/nginx/
sudo ln -s /etc/nginx/sites-available/"${WEB_NAME}".conf /etc/nginx/sites-enabled/"${WEB_NAME}".conf
sudo rm /etc/nginx/sites-enabled/default
sudo service nginx restart

# Install GRAV in webroot
sudo chown -R ${WWW_USER}:${WWW_USER} /var/www
sudo wget https://getcomposer.org/download/1.9.1/composer.phar -O /usr/local/bin/composer && sudo chmod 755 /usr/local/bin/composer
echo "Git"
git clone -b master https://github.com/getgrav/grav.git "${WEB_ROOT}""${WEB_NAME}"
sudo chown -R ${WWW_USER}:${WWW_USER} "${WEB_ROOT}"

echo "enter git"
cd "${WEB_ROOT}""${WEB_NAME}"
echo "composer"
sudo -u "${WWW_USER}" composer install --no-dev -o
sudo chown -R ${WWW_USER}:${WWW_USER} "${WEB_ROOT}"
sudo chmod 775 -R bin/
sudo -u "${WWW_USER}" bin/grav install
#sudo -u "${WWW_USER}" bin/gpm install disroot
#sudo -u "${WWW_USER}" bin/gpm install language-selector

sudo -u "${WWW_USER}" bin/gpm install themer
# https://github.com/sommerregen/grav-plugin-themer

sudo -u "${WWW_USER}" bin/gpm install form
sudo -u "${WWW_USER}" sed -i 's/quark/disroot/g' "${YAML}"

# Add Grav config (the second extra false should be set to true, but it creates an error in Grav ("Trying to access array offset on value of type null")

if ! grep -q auto_line_breaks "${YAML}" ; then
    sudo -u "${WWW_USER}" sed -i "s/extra:\ false/\extra:\ false\n\    auto_line_breaks:\ true\n\    auto_url_links:\ true\n\    escape_markup:\ false\n\    special_chars:\n\      \'\>\':\ \'gt\'\n\      \'\<\':\ \'lt\'/" "${YAML}"
fi

if ! grep -q languages "${YAML}" ; then
    sudo -u "${WWW_USER}" echo "
languages:
  supported:
    - en
    - es
    - fr
    - it
    - pt
    - de
    - ru
  include_default_lang: true
  pages_fallback_only: false
  translations: true
  translations_fallback: true
  session_store_active: true
  http_accept_language: true
  override_locale: false" >> "${YAML}"
fi

sudo -u "${WWW_USER}" ex -s -c "4i|  alias: '/home'" -c x "${YAML}"
sudo -u "${WWW_USER}" sed -i '4d' "${YAML}"

if ! grep -q redirect_default_route "${YAML}" ; then
    sudo -u "${WWW_USER}" ex -s -c '16i|  redirect_default_route: true' -c x "${YAML}"
fi

sudo -u "${WWW_USER}" -i 's/false/true/g' "${YAML}"


# Add website domain to local /etc/hosts file
sudo sed -i "s/127.0.0.1\tlocalhost/127.0.0.1\tlocalhost $WEB_NAME/" /etc/hosts

#Add website pages in GRAV
sudo mount -o bind /var/www/pages "${WEB_ROOT}""${WEB_NAME}"/user/pages
chown "${WWW_USER}":"${WWW_USER}" -R "${WEB_ROOT}""${WEB_NAME}"/user/pages

#Add Privacy Statement pages in GRAV
sudo mount -o bind /var/www/pages/privacy_policy/_pp "${WEB_ROOT}""${WEB_NAME}"/user/pages/privacy_policy/_pp
chown "${WWW_USER}":"${WWW_USER}" -R "${WEB_ROOT}""${WEB_NAME}"/user/pages/privacy_policy/_pp

#Add Disroot theme  in GRAV
sudo -u "${WWW_USER}" ln -s "${WEB_ROOT}"/grav-theme-disroot "${WEB_ROOT}""${WEB_NAME}"/user/themes/disroot

#Add Disroot Blog theme in GRAV
sudo -u "${WWW_USER}" ln -s "${WEB_ROOT}"/grav-theme-disrootblog "${WEB_ROOT}""${WEB_NAME}"/user/themes/disrootblog

#Add language-selector in GRAV
sudo -u "${WWW_USER}" ln -s "${WEB_ROOT}"/grav-plugin-language-selector "${WEB_ROOT}""${WEB_NAME}"/user/plugins/language-selector

exit 0
