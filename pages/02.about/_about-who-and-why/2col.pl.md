---
title: Email
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
body_classes: modular
wider_column: right
---

<a class="button button1" href="/mission-statement">Przeczytaj o naszej misji</a>

---

**Disroot** to projekt z siedzibą w Amsterdamie utrzymywany przez wolontariuszy i zależy od wsparcia społeczności.

Na początku stworzyliśmy **Disroota** aby zaspokoić własne potrzeby. Szukaliśmy oprogramowania którego moglibyśmy używać do komunikacji, dzielenia się i organizacji w naszych własnych kręgach. W większości dostępnych rozwiązań brakowało jednak tego, co uważaliśmy za ważne - nasze narzędzia powinny być **otwarte**, **zdecentralizowane**, **sfederowane** oraz **szanujące wolność** i **prywatność**.

Szukając takich narzędzi znaleźliśmy wiele ciekawych projektów, projektów które według naszych przekonań powinny być dostępne dla każdego, kto również ceni sobie podobne wartości. Postanowiliśmy więc powiązać ze sobą kilka aplikacji i podzielić się nimi z innymi. Tak właśnie powstał **Disroot**.

Poprzez utrzymywanie **Disroota** mamy nadzieję zmienić to, jak ludzie zwykle korzystają z internetu. Chcemy zachęcić ludzi aby **uwolnili się** z powszechnie używanych zamkniętych platform (ang. *Walled Garden*) i przekonali się do otwartych i etycznych alternatyw, na naszej lub innej plaftormie (lub nawet do prowadzenia własnej).

Razem możemy stworzyć sieć, która jest prawdziwie niezależna, skupionej na przynoszeniu korzyści użytkownikom zamiast na wykorzystywaniu ich.
