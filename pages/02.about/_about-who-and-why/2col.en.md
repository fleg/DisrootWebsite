---
title: Email
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
body_classes: modular
wider_column: right
---

<a class="button button1" href="/mission-statement">Read our mission statement</a>

---

**Disroot** is a project based in Amsterdam which is maintained by volunteers and depends on the support of its community.


We originally created **Disroot** out of personal need, we were looking for software we could use to communicate, share and organize within our circles. Most of the available solutions were missing the key elements that we find important; Our tools should be **open**, **decentralized**, **federated** and **respectful** towards **freedom** and **privacy**.

While searching for such tools, we found some really interesting projects, projects that we believe should be available to anyone who values similar principals. We therefore decided to bundle some applications together and share them with others. This is how **Disroot** got started.

By running **Disroot** we hope to change how people typically interact on the web. We want to encourage people to **break free** of the walled gardens of popular software and turn to open and ethical alternatives, may it be on our platform or on another (or you could even host your own).

Together we can form a network that is truly independent, focused on the benefit of the people rather than the exploitation thereof.
