---
title: Przejrzystość
fontcolor: '#555'
wider_column: right
bgcolor: '#fff'

---

# Przejrzystość i otwartość

![](about-trust.png)

---

<br>

W **Disroocie** używany w pełni wolnego i otwartego oprogramowania. To znaczy, że kod źródłowy (opis działania oprogramowania) jest dostępny publicznie. Każdy może współtworzyć poprawki do oprogramowania, oraz w każdym momencie można je sprawdzić i dokonać audytu - nie ma ukrytych "tylnych wejść" ani żadnych innych niepożądanych funkcjonalności.

Chcemy być w pełni przejrzyści i otwarci wobec ludzi korzystających z naszych usług, więc publikujemy informacje na temat aktualnego stanu projektu, statusu finansowego, naszych planów i pomysłów. Chcielibyśmy też usłyszeć wasze uwagi i sugestie, abyśmy mogli dostarczać możliwie najlepsze usługi dla wszystkich **Disrootersów**.
