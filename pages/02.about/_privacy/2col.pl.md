---
title: prywatność
fontcolor: '#555'
wider_column: left
bgcolor: '#fff'

---

<br>

Oprogramowanie może być dowolnie tworzone i dostosowywane. Każdy przycisk, kolor i odnośnik który widzimy i którego używamy w internecie został tam przez kogoś wstawiony. Kiedy używamy dostarczonych nam aplikacji zwykle nie zauważamy i nie przejmujemy się tym, co dzieje się za używanym przez nas interfejsem. Łączymy się z innymi ludźmi, przechowujemy swoje pliki, organizujemy spotkania i festiwale, wysyłamy emails lub rozmawiamiy z innymi godzinami, a wszystko w jakiś sposób się dzieje.
Przez ostatnie kilka dekad informacje stawały się coraz cenniejsze, i coraz łatwiejsze w zbieraniu i przetwarzaniu. Przyzwyczailiśmy się do bycia analizowanymi, ślepo godząc się na warunki użytkowania "dla naszego dobra", ufając rządom i wielkim korporacjom aby broniły naszych interesów, jednak przez cały czas to my jesteśmy produktem w ich "farmach ludzi".

**Bądź właścicielem swoich danych:**
Wiele serwisów zbiera i analizuje Twój sposób korzystania z nich, a następnie używa tych informacji by reklamować Ci różne produkty. **Disroot** nie używa Twoich danych do jakichkolwiek celów innych niż umożliwienie Ci korzystania z usług.
Twoje pliki w chmurze są zaszyfrowane za pomocą Twojego hasła, każdy pastebin i plik wysłany do usługi **Lufi** jest również zaszyfrowany po stronie klienta. To znaczy, że nawet administatorzy serwera nie mają dostęp do Twoich danych. Włączamy szyfrowanie gdzie tylko jest to możliwe, a gdy nie jest to możliwe, zalecamy korzystanie z zewnętrznych narzędzi szyfrujących. Im mniej my, jako administratorzy, wiemy o Twoich danych, tym lepiej :D (Porada dnia: *Nigdy nie zapomnij swojego hasła!*) [Zobacz samouczek](https://howto.disroot.org/en/tutorials/user/account/ussc)

---

# Prywatność


![](priv1.jpg?lightbox=1024)
![](priv2.jpg?lightbox=1024)
