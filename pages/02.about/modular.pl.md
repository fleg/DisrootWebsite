---
title: 'O Disroot'
bgcolor: '#1F5C60'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _about-who-and-why
            - _core-team
            - _federation
            - _privacy
            - _transparency
body_classes: modular
header_image: about-banner.jpeg
---
