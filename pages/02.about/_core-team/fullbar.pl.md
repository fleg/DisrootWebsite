---
title: 'Green bar'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: center
---


## Zespół Disroot

Disroot został założony przez **Antilopa** i **Muppeth**a w 2015.
W lipcu 2019 **Fede** i **Meaz** dołączyli do zespołu.

![antilopa](antilopa.png "Antilopa") ![muppeth](muppeth.png "Muppeth") ![fede](fede.png "Fede") ![meaz](meaz.png "Meaz")
