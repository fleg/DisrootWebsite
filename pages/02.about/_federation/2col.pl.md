---
title: Federacja i Decentralizacja
fontcolor: '#555'
wider_column: right
bgcolor: '#fff'

---

# Federacja i Decentralizacja

<br>

![](about-organize.jpg)

---

<br>

Większość usług internetowych opiera się na jednym, głównym miejscu będącym własnością korporacji. Przechowywane i analizowane są tam dane ich użytkowników celem utworzenia dokładnych profili. Te informacje są często używane celem wykorzystania użytkowników dla celów reklamodawczych, jak również mogą być pozyskane przez instytucje rządowe lub hackerów o złych zamiarach. Dane mogą być usunięte bez ostrzeżenia z niejasnych powodów lub lokalnych zasad dotyczących cenzury.

Usługa zdecentralizowana może działać na wielu urządzeniach, posiadanych przez różne osoby, firmy lub organizacje. Za pomocą protokołów federacji wszystkie te instancje mogą ze sobą współpracować i tworzyć jedna sieć składającą się z wielu węzłów (serwerów, na których uruchomione są podobne usługi). Chociaż każdy z takich węzłów z osobna może być wyłączony, to nie wpływa to na całą sieć. Dzięki temu cenzura jest praktycznie niemożliwa.

W ten sposób działa aktualnie poczta email - możesz skorzystać z usług dowolnego istniejącego dostawcy lub założyć swój własny serwer, a mimo to wciąż możesz wymieniać się emailami z użytkownikami innych serwerów. Email jest oparty o zdecentralizowany i federacyjny protokół.

Te dwazałożenia, razem, tworzą podstawę ogromnej sieci która opiera się na dość prostej infrastrukturze i relatywnie niskich kosztach. Każde urządzenie może stać się serwerem i stać się członkiem sieci na równych prawach. Od zwykłego domowego komputera, poprzez dedykowany serwer aż po całe szafy wypełnione wieloma serwerami. To podejście pozwala na stworzenie największej globalnej sieci będącej w posiadaniu użytkowników. Dokładnie tak miał wyglądać internet.

Nie chcemy, aby **Disroot** był jednym centralnym punktem, a jedynie częścią większej społeczności - jednym węzłem z wielu. Mamy nadzieję, że wiele innych osób będzie czerpać inspirację i stworzy więcej projektów o podobnych założeniach.
