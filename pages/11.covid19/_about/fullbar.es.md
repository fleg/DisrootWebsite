---
title: Kit Covid19
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
text_align: center
body_classes: modular
---

<br>
## ¡Brote de Coronavirus!
El brote de Coronavirus nos ha puesto a todxs en una situación singular. Una gran mayoría de la población del planeta debe permanecer en sus casas como medida de prevención para que la enfermedad no se siga expandiendo. Esto ha generado un colapso en un número de servicios que dependen de la internet y por eso, aquí en **Disroot**, decidimos colaborar poniendo a disposición algunas herramientas muy útiles así todxs pueden comunicarse, trabajar y compartir: el **Kit para el Covid-19 de Disroot**.

## Estos servicios no requieren registrarse

También queremos ayudar a otras organizaciones o pequeñas empresas proporcionando instancias privadas personalizadas por un costo accesible.
