---
title: Covid-19 Kit
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
text_align: center
body_classes: modular
---

<br>
## Pandemia koronawirusa!
Pandemia koronawirusa wciągnęła nas wszystkich w nietypową sytuację. Większość światowej populacji musi pozostać w domach, aby powstrzymać rozprzestrzenianie się wirusa. Spowodowało to problemy w wielu usługach, które polegają na internecie i dlatego w Disroocie postanowiliśmy pomóc udostępniając wszystkim kilka przydatnych narzędzi, aby wszyscy mogli się komunikować, współpracować i dzielić się: Zestaw Covid-19 od Disroota.

## Te usługi nie wymagają rejestracji.

Chcemy też pomóc innym organizacjom i małym firmom poprzez dostarczanie indywidualnie dostosowanych, prywatnych instancji za przystępną cenę.
