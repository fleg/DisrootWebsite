---
title: Services
section_id: services
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
services:
    -
        title: Pads
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Wspólnie twórz i edytuj dokumenty w przeglądarce, w czasie rzeczywistym."
        button: 'https://pad.disroot.org'
        buttontext: "Utwórz pad"
    -
        title: Calc
        icon: calc.png
        link: 'https://disroot.org/services/pads'
        text: "Wspólnie twórz i edytuj arkusze kalkulacyjne w przeglądarce, w czasie rzeczywistym."
        button: 'https://calc.disroot.org'
        buttontext: "Utwórz arkusz"
    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Szyfrowany pastebin/forum dyskusyjne."
        button: 'https://bin.disroot.org'
        buttontext: "Utwórz wklejkę"
    -
        title: Upload
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Szyfrowane, krótkoterminowe dzielenie się plikami."
        button: 'https://upload.disroot.org'
        buttontext: "Wyślij plik"
    -
        title: Ankiety
        icon: polls.png
        link: 'https://disroot.org/services/polls'
        text: "Narzędzie online do szybkiego i łatwego planowania spotkań i podejmowania decyzji."
        button: 'https://poll.disroot.org'
        buttontext: "Utwórz ankietę"
    -
        title: 'Wideorozmowy'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Narzędzie do wideorozmów."
        button: 'https://calls.disroot.org'
        buttontext: "Nowa rozmowa"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "Rozmowy głosowe cechujące się niskim opóźnieniem i wysoką jakością."
        button: 'https://mumble.disroot.org'
        buttontext: "Zaloguj się"
        badge: EXPERIMENTAL
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "Alternatywa dla popularnych narzędzi biurowych zaprojektowana z myślą o prywatności."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Access"
        badge: EXPERIMENTAL
    -
        title: 'Gry'
        icon: game.png
        link: 'https://disroot.org/en/quarantine'
        text: "Wspólna gra"
        button: 'https://disroot.org/en/quarantine'
        buttontext: "Więcej informacji"
----
