---
title: 'Uso di Nextcloud Deck'
media_order: deck_web_interface.jpg
published: true
date: '12-09-2020 00:00'
taxonomy:
    category:
        - news
    tag:
        - nextcloud
        - deck
        - tutorial
        - users experiences
body_classes: 'single single-post'
---

**Cari Disrooters**:

Dopo un utilizzo privato di diversi mesi di **Nextcloud Deck** e dopo che i bug più gravi sono stati corretti (c'erano alcuni problemi di interazione con la nuovissima app di **Android**), questo è certamente un buon momento per dare alla comunità una breve panoramica delle funzionalità di **Nextcloud Deck**.
---

# Cosa è Deck?
**Nextcloud Deck** è uno strumento di organizzazione in stile **KANBAN**. Iniziamo quindi a definire cos'è KANBAN.

Le sue origini derivano dal controllo del processo di produzione nell'industria ed è stato sviluppato da **Toyota** nel **1947** con l'obiettivo di aumentare la produttività aziendale. Si basa fondamentalmente su elenchi in cui le attività vengono collocate come carte.

Mentre il metodo agile sta diventando sempre più comune nello sviluppo del software, KANBAN è ancora il metodo standard negli impianti di produzione.

## A cosa assomiglia?

![](en/deck_web_interface.jpg)

## Quali caratteristiche ha?

* Aggiungi le tue attività alle carte (task) e mettile in ordine
* Assegna un task ad un utente
* Assegna dei label per una maglior organizzazione
* Scrivi ulteriori informazioni in markdown
* Condividi i task con il tuo team, i tuoi amici o la tua famiglia
* Allega dei file e integrali nella descrizione scritta in markdown
* Discuti con il tuo team usando i commenti
* Tieni traccia dei cambiamenti e delle attività più importanti

# Per iniziare

Nel setup di default di **Disroot** l'app **Deck** non è presente nel menu delle applicazioni.

![](en/navbar.png)

Per aggiungerlo fai clic sulla tua immagine di profilo presente in alto a destra e scegli "Impostazioni".<br>
Quindi vai su "Ordine app" e seleziona la casella di controllo **Deck**.

![](en/app_order.gif)

Sappi che puoi decidere l'ordine delle app semplicemente trascinandole.

![](en/navbar_deck.png)

Qui è presente una guida che mostra le più importanti funzionalità di **Deck**.<br>
- [Guida alle funzionalità di Deck](https://deck.readthedocs.io/en/latest/User_documentation_en/) (Attenzione, l'interfaccia grafica presente nella guida fa riferimento ad una versione vecchia di Nextcloud.)

## Android app

Con l'app di **Android** puoi accedere e organizzare i tuoi progetti in qualsiasi momento.<br>
Nell'app sono infatti presenti tutte le funzioni presenti nell'interfaccia web.

![](en/deck_android_app.jpg)

**Provala**

![https://f-droid.org/en/packages/it.niedermann.nextcloud.deck/](en/f-droid.png)


## Cosa manca?

Non manca molto. Per progetti più grandi e complessi, sarebbe ovviamente auspicabile la possibilità di modificare l'impostazione delle notifiche in modo più specifico.<br>
  - [Possibilità di scegliere le specifiche per le notifiche](https://github.com/nextcloud/deck/issues/713)<br>

Sarebbe inoltre interessante se nel calendario venissero mostrate i task con una data di scadenza.<br>
  - [Integrazione con Nextcloud CalDAV API](https://github.com/nextcloud/deck/issues/15)


## E per finire...

Uso **Deck** da sei mesi ormai per coordinare piccoli progetti e attività nella mia vita quotidiana. Funziona molto bene per me e questa app merita sicuramente di essere provata.
**Un caro saluto,<br>
avg_joe**<br>
(liberamente tradotto da l3o)


### Fonti e collegamenti:

- [App Deck di Nextcloud](https://apps.nextcloud.com/apps/deck)
- [Documentazione su Nextcloud](https://deck.readthedocs.io/en/latest/User_documentation_en/)
- [Codice sorgente di Nextcloud Deck](https://github.com/nextcloud/deck)
- [F-Droid Android App](https://f-droid.org/en/packages/it.niedermann.nextcloud.deck/)
- [KANBAN su **Wikipedia**](https://en.wikipedia.org/wiki/Kanban)
