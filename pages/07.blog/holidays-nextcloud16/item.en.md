---
title: 'Holidays, Nextcloud 16, Disroot Discover'
date: '03-06-2019 11:00'
media_order: holidays.jpg
taxonomy:
  category: news
  tag: [disroot, news, donations, etherpad, nextcloud]
body_classes: 'single single-post'

---

## Holidays
Sandy beaches, heat and warm sea. Yup. For the past two weeks Disroot team was enjoying holidays. We've got sun burned, our skin peeled off and we got sunburned again. We had tons of fun and most importantly re-charged our batteries, rested and gotten a lot of ideas and energy for the next months to come. If you thought however we have not worked at all during that time (we wish) you are wrong. We did some work here an there, enjoying warm evenings and cold drinks.
<br><br>
We had to rebuild our backup system in order to make it more performant. With the increase of data being backed up on daily basis we had to take another approach in daily backups. Rather then backing up everything at once, we now broken the backups into smaller chunks which makes it easier to and faster. We are happy with the setup and looking forward to additional improvements we want to implement soon.

## Nextcloud 16
While on holidays we've gotten notification that Nextcloud 16.0.1 is up and ready to roll out. The latest release of Nextcloud bring so many cool new features we couldn't wait and decided to roll it out promptly to all disrooters to enjoy. So whats new? Below are some of the highlights. For more details on new features in Nextcloud 16, check their [blog post](https://nextcloud.com/blog/nextcloud-16-introduces-machine-learning-based-security-and-usability-features-acl-permissions-and-cross-app-projects/)

  - **Projects** - you can now create projects which can be linked to files, Talk (chat app) and deck.
  - **Talk** - possibility to mention everyone in the room, command that allow you to create user defined actions, various user experience improvements as well as Android and iOS apps
  - **Deck** - share boards with your Circles, Integration with projects
  - Overall **performance** improvements and **bug fixes**

## New Etherpad theme
We have also enabled the new experimental theme on Etherpad. It looks sleek and gives fresh look that Etherpad was screaming for years for a bit of an uplift. Thanks for new theme and keep up the good work Etherpad team!
![](etherpad.png)

## Mission statement
While sipping delicious Gin and Tonic we've been putting together something that was on our ToDo list for way over a year now and it's something we really needed to have defined in order to provide better understanding of what Disroot is, what we stand for, what our aspirations are and how we work. We are currently running it through some friends for proof reading, giving it last polishes and fixes. We plan to publish it within the next two weeks.

## Update to DisApp
While we were burning in the sun, massimiliano decided to work on improvements on DisApp. New DisApp (find it on [fdroid](https://f-droid.org/en/packages/org.disroot.disrootapp/) ) introduces Disroot state notifications. Disroot state ([https://state.disroot.org](https://state.disroot.org) ) is a site where we publish scheduled maintenance and downtimes as well as issues, errors, software updates and other information about the health and state of the platform. You can follow it via RSS feed, email notifications as well as Hubzilla, diaspora*, Mastodon (fediverse in general) as well as Jabber/XMPP and Matrix. From now Disapp by massimiliano is added to this list. From new version 1.2.0 on, your phone will automatically inform you about new postings on Disroot state site. You will never miss info about when we are planning to reboot servers, or when things are broken and not working (and most importantly why).

Additionally for all those who like to see more detailed uptime information we have provided an additional board (powered by uptimerobot) via button on the bottom of [state](https://state.disroot.org) page or directly through [https://uptime.disroot.org](https://uptime.disroot.org)

## Discover
This is a new experiment we would like to try out. Disroot's community is full of amazing and interesting people and projects. In order to help those to create bigger communities, we've decided to dedicate a space in our regular blogposts for promoting interesting things disrooters are involved in.  We want to help you guys create and broaden your communities and project as well as reach wider audience by using our platform. If you have anything interesting you would like to share with others,  drop us a line at email: **discover_at_disroot.org**. We will then review it and publish when compiling new blog posts such as this one. (Remember we only promote non-profit and "copyleft" projects and work).

Our little platforms is fueled by coffee. Please consider becoming our regular contributor donating an equivalent of your favorite coffee (or other beverage). The entire project depends on your donations. Visit [our donation page](https://disroot.org/donate) to read more about ways you can donate and to check the financial overview.
