---
title: 'New energy, New projects'
date: '2-03-2019 9:12'
media_order: flower.jpg
taxonomy:
  category: news
  tag: [disroot, news, matrix, donations, report]
body_classes: 'single single-post'

---

We begun the new year with a big kick. The amount of work both private, professional and Disroot related has become huge.  We are psyched and excited about all the things in our lives so we wanted to share with you what is going on and what we are busy with these days.
<br><br>
Our daughter project [apt-get shirt](https://aptgetshirt.com) is progressing slowly but surely. Last year we decided that outsourcing the shirt printing process is way too expensive for the amounts of shirts we sell and so we decided to build our own production facilities. We have finally finished building a DIY silkscreen printing press and are looking forward to the moment we can self-produce shirts for all the **free/libre/open source software** projects out there. You can follow **"apt-get shirt"** on the fediverse on: [mastodon](https://social.tchncs.de/@aptgetshirt), [hubzilla/Diaspora/Friendica](https://hub.disroot.org/channel/aptgetshirt) as well as evil [twitter](https://twitter.com/aptgetshirt) if you must. Also while you're at it, grab some shirts from our [web store](https://aptgetshirt.com), our closet is still full of shirts from the first badge.
<br><br>
Our life project has become real too. Both *Muppeth* and *Antilopa* are going to build an Open Source house with their own hands. We are going to use pioneering building technique called [wikihouse](https://wikihouse.cc/) ( [video](https://www.youtube.com/watch?v=4fB3SFgKPog) ) which is totally open source technology. @antilopa for last weeks has been busy preparing all drawings and plans for our future home. We are going to document the entire process, including building plans, files ready to cut on CNC and assemble the house, the entire process of building the house, etc). You can follow us on the [fediverse](https://hub.disroot.org/channel/wikidom) (wikidom@hub.disroot.org)
<br><br>
@muppeth has been busy at his dayjob last weeks migrating the entire organization he works for to Nextcloud. Yup baby! this is contagious and spreading. It's a very good experiment from our point of view to experience and see how Nextcloud, with its amazing apps suit for office use, will fit the needs of group management in a proper job setting as a replacement to corporate solutions. So far the general feedback is very positive.
<br><br>
As you can see crazy amount of exciting things. That does not mean nothing is happening with Disroot. And since this is what it's all about, let's see whats cookin'

### Nextcloud 15.

Those of you who use it probably noticed one or two adjustments to the design of the cloud. Yes, Nextcloud’s interface is evolving. The most notable differences are the addition of a grid view in Files (especially useful when dealing with images) and the new top bar. On the security side of things Nextcloud now provides phone notification as second factor (approve a login on your phone or desktop client!) which can be selected in your options.
<br><br>
Sharing has also gotten a number of new features and design changes.
  - Unlimited amount of share links - create multiple share links to your folder or file to keep better control over it.
  - View-only documents. - to prevent others from editing and even downloading files.
  - Talk integration - Now you can start a video call or a chat in connection to the file you are working on directly from the file menu.

### New howto.disroot.org structure.
Together with few disrooters that are active in writing and translating tutorials we decided to change the general structure of the [howto](https://howto.disroot.org) website. This was an outcome of our first howto-team conference. The call utilized 100% Disroot infrastructure, which we are very excited about (stretching from Europe to South America). We used Jitsi which you can find and use under [https://calls.disroot.org](https://calls.disroot.org) . Jitsi will be added to the website soon.<br>
Fede has come up with a new structure that we believe will help find needed information and offer a clear overview. We just rolled it out and will now focus on defining what howtos are missing, which ones are outdated and start scheduling the work. To keep track of things we will utilize a Taiga board so anyone can join in, pick up a task and help out. If you want to get in touch with other disrooters working on tutorials and translations you should head on to our jabber channel at xmpp:howto@chat.disroot.org or via our experimental [webchat](https://webchat.disroot.org/#converse/room?jid=disroot@chat.disroot.org) (something we want to fix up in coming days).<br>
We have also started discussing how could we contribute our work upstream. Our howto website covers a lot of services and it would be a waste if it would have been only exclusive to Disroot users. With streamlining and unifying the texts we hope to develop an easy way for anyone out there to transform those howtos for their own project or needs (service providers, project pages etc)
Thanks Fede for amazing job and commitment!

### Whats next?
To begin with, we will continue to cleanup and work on internal improvements to the setup. We have answered all questions pending our support queue which due to high workloads kept piling up for weeks (sorry if you happen to be a victim of such delay), we are reviewing, updating and re-planing our Taiga board to be more efficient and transparent.
We are also focusing on streamlining configurations, dealing with pending issues and improvements. And although we are slightly delayed, we are on track with preparations for service-side mailbox encryption.
<br><br>
Cheers!
