---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "KOMUNIKACJA"
section_number: "100"
faq:
    -
        question: "Nie mogę połączyć się z Matriksem. Dlaczego?"
        answer: "<p>Od Sierpnia 2018 postanowiliśmy zakończyć utrzymywanie naszej instancji <b>Matriksa</b>. Powody możesz przeczytać <a href='https://disroot.org/en/blog/matrix-closure' target='_blank'>tutaj</a>.</p>
        <p>Możesz też znaleźć więcej informacji na temat problemów z prywatnością w Matriksie <a href='https://github.com/libremonde-org/paper-research-privacy-matrix.org' target='_blank'>tutaj</a></p>"
    -
        question: "Jak mogę używać Disrootowego IRCa w pokojach XMPP?"
        answer: "<p><b>Disroot</b> dostarcza tak zwany transport IRC.
        To znaczy, że możesz dołączyć do dowolnego pokoju IRC znajdującego się na dowolnym serwerze. Adresy XMPP wyglądają w ten sposób:</p>
        <ol>
        <li>Aby dołączyć do pokoju IRC: <b>#pokój%irc.domain.tld@irc.disroot.org</b></li>
        <li>Aby dodać użytkownika IRC: <b>użytkownik%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Gdzie <b>#pokój</b> jest pokojem IRC, do którego chcesz dołączyć, a <b>irc.domain.tld</b> jest adresem serwera, gdzie znajduje się pokój. Upewnij się, że <b>@irc.disroot.org</b> znajduje się w adresie XMPP, bo jest to adres Disrootowego transportu IRC<->XMPP.</p>"
    -
        question: "Nie mogę połączyć się z usługą Diaspora* za pomocą mojego konta Disroot."
        answer: "<p>Ponieważ <b>Diaspora*</b> nie wspiera <b>LDAP</b> (protokołu, którego używamy aby dawać dostęp do usług za pomocą pojedyńczego konta) zalogowanie się za pomocą konta <b>Disroot</b> nie jest możliwe. Musisz stworzyć kolejne konto <a href='https://pod.disroot.org/' target='_blank'>tutaj</a>. Niestety, rejestracja jest aktualnie wyłączona.</p>"

---
