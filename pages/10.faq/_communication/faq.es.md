---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "SOBRE COMUNICACIÓN"
section_number: "100"
faq:
    -
        question: "No puedo conectarme a Matrix. ¿Por qué?"
        answer: "<p>En septiembre de 2018 decidimos discontinuar <b>Matrix</b>. Puedes leer las razones <a href='https://disroot.org/en/blog/matrix-closure' target='_blank'>aqui</a>.</p>
        <p>Puedes encontrar más información sobre el impacto en la privacidad de Matrix <a href='https://github.com/libremonde-org/paper-research-privacy-matrix.org' target='_blank'>aqui</a></p>"
    -
        question: "¿Cómo puedo utilizar el IRC de Disroot en las salas XMPP?"
        answer: "<p><b>Disroot</b> proporciona la llamada puerta de enlace IRC. Esto significa que puedes unirte a cualquier sala IRC hospedada en cualquier servidor IRC. Este es el esquema de dirección básico:</p>
        <ol>
        <li>Para unirte a una sala IRC: <b>#sala%irc.domain.tld@irc.disroot.org</b></li>
        <li>Para agregar un contacto IRC: <b>nombre_del_contacto%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Donde <b>#sala</b> es la sala IRC a la que quieres unirte y <b>irc.domain.tld</b> es la dirección del servidor. Asegúrate de dejar <b>@irc.disroot.org</b> como está porque es la dirección del enlace IRC.</p>"
    -
        question: "No puedo conectarme a Diaspora* con mi cuenta dey Disroot."
        answer: "<p>Como <b>Diaspora*</b> no soporta <b>LDAP</b> (el software de protocolo que utilizamos para acceder a los servicios con una sola cuenta) no es posible iniciar sesión con las credenciales de <b>Disroot</b>. Necesitas crear una cuenta diferente <a href='https://pod.disroot.org/' target='_blank'>aquí</a>. However, registrations are currently closed.</p>"

---
