---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "SZYFROWANIE"
section_number: "200"
faq:
    -
        question: "Czy mogę używać dwustopniowego logowania?"
        answer: "<p>Tak, możesz używać go dla <b>Chmury</b>. Ale zanim je włączysz, upewnij się, że rozumiesz jak działa i jak go używać. Aby uzyskać więcej informacji, kliknij <a href='https://howto.disroot.org/pl/tutorials/cloud/introduction#two-factor-authentication' target='_blank'>tutaj</a></p>"
    -
        question: "Czy mogę używać szyfrowania end-to-end w chmurze?"
        answer: "<p>Aktualnie szyfrowanie end-to-end jest wyłączone w związku z błędem w aplikacji <b>Nextclout</b>.</p>"


---
