---
title: 'Goal'
bgcolor: '#09846aff'
fontcolor: '#FFFFFF'
text_align: left
---

<br>


# Szybki przewodnik:
1. Na początek zainstaluj Teeworlds za pomocą menedżera pakietów (jeśli używasz poważnego systemu operacyjnego, jak Linux czy BSD), albo odwiedź [stronę internetową Teeworlds](https://teeworlds.com/?page=downloads) i pobierz ją dla swojej platformy.
2. Odpal grę.
3. Wybierz "Setup" aby ustawić nazwę gracza i dostosować wygląd swojego bohatera.
4. Rozpocznij grę, znajdź **Quarantine with Disroot** i dołącz do nas!
![](splashtee3.png)
