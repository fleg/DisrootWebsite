---
title: 'Are you bored yet'
bgcolor: '#7b384fff'
fontcolor: '#FFF'
text_align: left
---

<br>

<span style="padding-right:10%;">
## Nudzi Ci się już na kwarantannie?

Ze względu na to, że wielu z was (my również) musi siedzieć w domach i unikać kontaktu z innymi ludźmi, uznaliśmy, że fajnie będzie spędzić trochę czasu spotykając się z innymi ludźmi znanymi Ci z czatów, sfederowanych sieci społecznościowych i których najprawdopodobniej nigdy wcześniej w życiu nie spotkałeś/aś.

Spędźmy popołudnie na piciu browarów, jedzeniu śmieciowego żarcia i strzelając do siebie w Teeworlds. Pierwsza edycja Kwarantanny z Disrootem odbędzie się w sobotę 21.03.2020 o 20:00 CET, i będzie trwać do ostatniej osoby. Serwer będzie prawdopodobnie działał przez tydzień czy dwa, jeśli ktoś będzie chciał grać.

</span>

<span style="padding-right:30%;">
</span>
