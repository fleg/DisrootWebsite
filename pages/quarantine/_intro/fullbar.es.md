---
title: 'Ya están aburridxs'
bgcolor: '#7b384fff'
fontcolor: '#FFF'
text_align: left
---

<br>

<span style="padding-right:10%;">
## ¿Ya están aburridxs?

Ya que muchxs de ustedes (nosotrxs incluidxs) están encerradxs en sus casas tratando de evitar el contacto con otxs seres humanxs, pensamos que estaría bueno pasar este tiempo socializando frente a la computadora con un montón de extrañxs que conoces tan bien de los chats y las redes sociales federadas y a quienes muy probablemente nunca hayas visto en la vida real.

Pasemos una noche bebiendo alcohol, comiendo basura y disparándonos unxs a otrxs en **Teeworlds**. La primera edición de **Cuarentena con Disroot** comienza el sábado 21.03.2020 a las 20:00 CET y durará hasta que quede solo unx en pie. El servidor estará funcionando toda la semana o dos si hay personas con ganas de jugar.

</span>

<span style="padding-right:30%;">
</span>
