---
title: Warunki użytkowania
bgcolor: '#FFF'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _title
            - _tos
body_classes: modular
header_image: havefun.jpg
---
