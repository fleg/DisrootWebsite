---
title: 'Goals'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
goals:
    -
        title: 'Compartiendo nuestra buena fortuna'
        text: "Si recibimos al menos 400 EUR en donaciones, compartimos el 15% de nuestro excedente con lxs desarrolladores del software que utilizamos. Disroot no existiría sin ellxs."
        unlock: yes
    -
        title: 'Pagar una Cuota por Voluntariado'
        text: "Si nos quedan más de 140€ al final del mes, pagamos a un miembro del Equipo Central de Disroot una cuota por voluntariado de 140€."
        unlock: yes
    -
        title: 'Pagar dos Cuotas por Voluntariado'
        text: "Si nos quedan más de 280€ al final del mes, pagamos a dos miembros del Equipo Central de Disroot una cuota por voluntariado de 140€."
        unlock: yes
    -
        title: 'Pagar tres Cuotas por Voluntariado'
        text: "Si nos quedan más de 420€ al final del mes, pagamos a tres miembros del Equipo Central de Disroot una cuota por voluntariado de 140€."
        unlock: no
    -
        title: 'Pagar cuatro Cuotas por Voluntariado'
        text: "Si nos quedan más de 560€ al final del mes, pagamos a cuatro miembros del Equipo Central de Disroot una cuota por voluntariado de 140€."
        unlock: no
---

<div class=goals markdown=1>

</div>
