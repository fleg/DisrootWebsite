---
title: 'Rapports annuels'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: center
---
<br>

# Rapports annuels:

**Rapport annuel 2018** <br>
[Repport 2018](https://disroot.org/annual_reports/AnnualReport2018.pdf)

**Août 2016 - Août 2017** <br>
[Rapport 2016-2017](https://disroot.org/annual_reports/2017.pdf)

**2015 - Août 2016** <br>
Coûts: €1569.76 <br>
Dons: €264.52

---



---
<br>

# Dons de matériel informatique
Si vous avez du matériel qui pourrait être utilisé pour notre projet (Mémoire, serveurs, processeurs, disques durs, cartes réseaux/Raid, équipement réseau, etc.) contactez-nous. Tout matériel informatique est le bienvenu. Les choses qu'on ne peut pas utiliser peuvent être vendues pour gagner de l'argent.
Nous recherchons également des propositions d'espace rack.
