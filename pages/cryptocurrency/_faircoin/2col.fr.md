---
title: Faircoin
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

![](faircoin-qr.png)

---

# Faircoin
Envoyez vos faircoins à notre porte-monnaie:
fLtQUZWMBiYkEneKb1hqTDoyoqzCkjL2qY
