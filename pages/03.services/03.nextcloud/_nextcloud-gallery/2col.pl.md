---
title: 'Nextcloud Galleries'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-gallery.png?lightbox=1024)

---

## Galleries

Share photo galleries with friends and family. Give them access to upload pictures, view and download them. Send a link to whoever you choose, and control whether they can share those photos with anyone else.
