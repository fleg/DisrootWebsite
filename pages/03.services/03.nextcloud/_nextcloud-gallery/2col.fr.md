---
title: 'Galeries Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-gallery.png?lightbox=1024)

---

## Galeries

Partagez des galeries de photos avec vos amis et votre famille. Donnez-leur l'accès pour télécharger des photos, les visionner et les télécharger. Envoyez un lien à la personne de votre choix et contrôlez si elle peut partager ces photos avec quelqu'un d'autre.
