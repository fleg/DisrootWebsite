---
title: 'Nextcloud - condivisione'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-files-comments.png?lightbox=1024)

---

## Condivisione e controllo di accesso

La semplice interfaccia grafica permette di condividere file con altri utilizzatori di **Disroot**, i quali a loro volta hanno la possibilità di caricare sul tuo cloud altri file. Il sistema di notifiche permette di essere sempre aggiornati su quanto sta avvenendo.
