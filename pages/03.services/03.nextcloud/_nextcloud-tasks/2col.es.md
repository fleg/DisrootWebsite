---
title: 'Tareas Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Tareas

Organiza, gestiona y sincroniza tus tareas entre todos tus dispositivos.

---

![](en/OCtasks.png?lightbox=1024)
