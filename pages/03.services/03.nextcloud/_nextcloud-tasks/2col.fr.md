---
title: 'Tâches Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Tâches

Organisez et gérez vos tâches. Grâce à la norme ouverte caldav standard, vous pouvez synchroniser vos tâches sur tous vos appareils.

---

![](en/OCtasks.png?lightbox=1024)
