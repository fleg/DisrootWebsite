---
title: 'Espace de stockage supplémentaire'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Espace de stockage supplémentaire

Il est possible d'étendre votre stockage cloud à 14, 29 ou 54 Go pour un coût de 0,15 euro par Go et par mois. Vous pouvez également choisir d'utiliser une partie de l'espace supplémentaire pour le stockage du courrier électronique, veuillez le spécifier dans la section commentaire du formulaire de demande.

Pour demander un espace de stockage supplémentaire, vous devez remplir ce  [formulaire](https://disroot.org/en/forms/extra-storage-space).<br>


---

<br><br>

<a class="button button1" href="https://disroot.org/en/forms/extra-storage-space">Demande de stockage supplémentaire</a>
