---
title: 'Marcadores'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Elige tu cliente favorito'
clients:
    -
        title: Bookmarks
        logo: en/bookmarks_logo.png
        link: https://f-droid.org/en/packages/org.schabi.nxbookmarks/
        text:
        platforms: [fa-android]
---

## Marcadores

Un administrador de marcadores para **Nextcloud**. Guarda tus páginas favoritas y sincronízalas con todos tus dispositivos.

---

![](en/Bookmarks.png?lightbox=1024)
