---
title: 'End to End'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

![](en/endtoend-android-nw.png?lightbox=1024)

---

## Crittografia end to end

La crittografia end to end utilizzata da **Nextcloud** offre la miglior protezione dei tuoi dati. Cartelle e file possono essere condivise con altri utenti e sincronizzati su più servizi senza che siano leggibili sul server.

<span style="color:red">**La crittograzia end-to-end si trova ancora in uno stato alpha, utilizzarla quindi con precauzione!**</span>

Maggiori informazioni si possono trovare al seguente indirizzo https://nextcloud.com/endtoend/
