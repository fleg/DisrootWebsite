---
title: 'Encuestas'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](polls.png)

## Encuestas cifradas de extremo-a-extremo
**Encuestas** te ofrece la posibilidad de crear y compartir encuestas completamente cifradas.


---
![](presentation.png)

## Presentaciones cifradas al vuelo
Crea presentaciones cifradas de extremo-a-extremo con un editor sencillo y junto a tus amigas, amigos, compañeros y compañeras de organización o trabajo. Tu presentación finalizada puede ser "reproducida" directamente desde CryptPad.
