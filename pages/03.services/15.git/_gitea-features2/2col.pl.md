---
title: 'Git Features'
wider_column: left
---

![](en/gitea_dashboard.png?lightbox=1024)

![](en/gitea_activity.png?lightbox=1024)


---

## Śledzenie zgłoszeń
Zarządzaj zgłoszeniami, prośbami o nową funkcjonalność oraz opiniami o Twoim projekcie.

## Tagi, osoby przypisane, kamienie milowe, komentarze
Skutecznie zarządzaj swoim repozytorium poprzez łatwe ustalanie kamieni milowych, sortowanie zgłoszeń, dyskutowanie i przypisywanie zgłoszeń do innych członków zespołu.

## Wiki
Używając Markdowna stwórz wiki opisujące Twój projekt.

## Aplikacja mobilna
Zarządzaj projektem w drodze za pomocą aplikacji mobilnej na Androida (dostępnej na F-Droid).
