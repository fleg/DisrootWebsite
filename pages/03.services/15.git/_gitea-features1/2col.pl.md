---
title: 'Git Features'
wider_column: left
---

## Łatwy w obsłudze
Podejrzyj publiczne repozytoria, znajdź to, nad którym chcesz pracować i sklonuj je na swoim komputerze. Albo stwórz swoje własne! Możesz skonfigurować je tak, aby było prywatne lub publiczne.

## Szybkie i lekkie
Gitea jest bardzo szybka i lekka. Nie ma przerośniętych, niepotrzebnych funkcjonalności. Tylko to, co jest Ci potrzebne do pracy nad Twoim otwartoźródłowym projektem!

## Powiadomienia
Otrzymuj maile kiedy zgłoszenia zostają rozwiązane, oczekujące zmiany zostaną dodane itp.

## Bezpieczeństwo
Używaj klucza ssh, autoryzacji dwustopniowej i klucza GnuPG, aby zabezpieczyć Twoje repozytorium.


---

![](en/gitea_explore.png?lightbox=1024)

![](en/gitea_secure.png?lightbox=1024)
