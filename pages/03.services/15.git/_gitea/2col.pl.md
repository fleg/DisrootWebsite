---
title: Git
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://git.disroot.org/user/sign_up">Zarejestruj się</a>
<a class="button button1" href="https://git.disroot.org/">Zaloguj się</a>

---

![gitea_logo](gitea.png?resize=100,100)


## Gitea

**Git Disroota** działa w oparciu o **Gitea**. **Gitea** jest tworzonym przez społeczność, potężnym, łatwym w obsłudze i lekkim narzędziem do hostowania kodu i współpracy w projekcie. Jest zbudowany wokół narzędzia kontroli wersji `git`, najpopularniejszym tego rodzaju narzędziem obecnie na świecie.

Disroot Git: [https://git.disroot.org](https://git.disroot.org)

Strona projektu: [https://gitea.io](https://gitea.io)

Kod źródłowy: [https://github.com/go-gitea/gitea](https://github.com/go-gitea/gitea)

<hr>

Na [git.disroot.org](git.disroot.org) potrzebujesz osobnego konta. Konta **Disroot** nie są tam wspierane.
