---
title: 'XMPP Settings'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

## Server settings
### XMPP ID:<span style="color:#8EB726"> <i>yourusername</i>@disroot.org </span>
##### Advanced settings: server/port<span style="color:#8EB726"> disroot.org/5222 </span>

---

#### Upload file size limit:<span style="color:#8EB726"> 10MB </span>
#### Archived messages expire after <span style="color:#8EB726"> 6 month </span>
<br>
