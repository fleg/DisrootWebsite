---
title: Discourse
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Inscribirse en Disroot</a>
<a class="button button1" href="https://forum.disroot.org/">Iniciar sesión o crear una cuenta solo para el Foro</a>

---
![](discourse_logo.png)

## Una aproximación moderna a los foros de discusión

El foro de **Disroot** está desarrollado por **Discourse**, una completa solución de código abierto para foros de discusión. Ofrece todo lo que tu comunidad, grupo o colectivo necesita para crear su plataforma de comunicación, tanto pública como privada. También le da la posibilidad a particulares de crear tópicos sobre temáticas de su interés y encontrar a otras personas con quienes discutirlas, o simplemente unirse a una comunidad existente.

Foro de Disroot: [https://forum.disroot.org](https://forum.disroot.org)

Página del proyecto: [http://discourse.org](http://discourse.org)

Código fuente: [https://github.com/discourse/discourse](https://github.com/discourse/discourse)
