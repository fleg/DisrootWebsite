---
title: 'Ustawienia skrzynki pocztowej'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
text_align: center
---

<br><br>
##### IMAP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Port <span style="color:#8EB726">993</span> | Uwierzytelnienie: <span style="color:#8EB726">Zwykłe hasło</span>
##### SMTP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">STARTTLS</span> Port <span style="color:#8EB726">587</span> | Uwierzytelnienie: <span style="color:#8EB726">Zwykłe hasło</span>
##### SMTPS: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">TLS</span> Port <span style="color:#8EB726">465</span> | Uwierzytelnienie: <span style="color:#8EB726">Zwykłe hasło</span>
##### POP: <span style="color:#8EB726">disroot.org</span> | <span style="color:#8EB726">SSL</span> Port <span style="color:#8EB726">995</span> | Uwierzytelnienie: <span style="color:#8EB726">Zwykłe hasło</span>

---

**Rozmiar skrzynki pocztowej:** 1GB
**Limit rozmiaru załączników:** 50MB

---

**Separatory:** Możesz używać separatorów (znak plusa, "+") w Twoim adresie email aby tworzyć podadresy, takie jak **adres+cokolwiek@disroot.org**, na na przykład by ułatwić filtrowanie i śledzenie spamu. Na przykład: david@disroot.org może stworzyć adres taki jak 'david+bank@disroot.org', który poda w swoim banku. Takich adresów można używać **tylko do odbierania poczty**, wysyłanie nie jest możliwe. Wysłane emaile zawsze będą docierały z adresu david@disroot.org.
