---
title: 'Email Clients'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

<br>
# Używaj swojego ulubionego klienta email
Istnieje wiele klientów email na komputery i telefony komórkowe. Wybierz ten, który najbardziej Ci się podoba.</br>

Zobacz naszą [stronę z instrukcjami](https://howto.disroot.org/en/tutorials/email/clients) aby dowiedzieć się więcej.
<br>
*Na górze tej strony znajdują się informacje konfiguracyjny, które pomogą Ci skonfigurować Twoje konto email z* **Disroot** *na Twoim urządzeniu. To są standardowe informacje, których potrzebuje Twój klient email, aby móc dotrzeć do serwerów* **Disroot** *a.*
