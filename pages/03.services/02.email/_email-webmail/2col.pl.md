---
title: 'Email Webmail'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

## Webmail

![webmail](en/webmail.png)

---

<br>
Możesz korzystać ze swojej skrzynki pocztowej za pomocą dowolnego urządzenia korzystając z naszego klienta webmail pod adresem [https://mail.disroot.org](https://mail.disroot.org)

Nasz webmail korzysta z **RainLoop**. Rainloop to nowoczesne podejście do webmaila. Jest prosty, szybki i posiada większość funkcjonalności, której można oczekiwać od webmaila:

<ul class=disc>
<li>Skórki: Wybierz obrazek tła.</li>
<li>Wątki: Wyświetlaj konwersacje w widoku wątków.</li>
<li>Tłumaczenia: Interfejs jest przetłumaczony na wiele języków.</li>
<li>Filtry: Ustaw reguły, według których poczta będzie automatycznie przenoszona lub kopiowana do odpowiednich folderów, usuwana albo przesyłana dalej.</li>
<li>Foldery: Zarządzaj swoimi folderami (twórz, usuwaj lub ukrywaj).</li>
<li>Szyfrowanie GPG: </b>RainLoop</b> zawiera wbudowaną opcję szyfrowania.</li>
<li>Kontakty: Możesz dodawać swoje kontakty, a nawet powiązać książkę adresową z kontaktami w Nextcloud.</li>
</ul>

**UWAGA**: *Proszę pamiętać, że szyfrowanie odbywa się po stronie serwera. To oznacza, że nie masz kontroli nad swoim kluczem prywatnym. Nie jest to tak bezpieczne, jak używanie zewnętrznego klienta poczty, takiego jak Thunderbird, gdzie szyfrowanie odbywa się na Twoim komputerze. Koniec końców jest to jednak lepsze niż brak jakiegokolwiek szyfrowania...*

Strona projektu: [https://www.rainloop.net](https://www.rainloop.net/)
