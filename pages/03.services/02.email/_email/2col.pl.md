---
title: Email
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Zarejestruj się w Disroot</a>
<a class="button button1" href="https://mail.disroot.org/">Zaloguj się</a>

---

## E-mail

**Disroot** dostarcza bezpieczne skrzynki email, z których możesz korzystać za pomocą własnego klienta lub przez przeglądarkę. Komunikacja między Tobą a serwerem jest zaszyfrowana za pomocą SSL. Dodatkowo, wszystkie maile podczas wysyłania z naszego serwera również są zaszyfrowane (TLD) jeśli serwer email odbiorcy wspiera taką funkcjonalność. To znaczy, że emaile nie są już wysyłane jako tradycyjna "pocztówka", ale znajdują się w czymś w rodzaju "koperty".

**Mimo to, zachęcamy Cię aby zawsze uważać w trakcie korzystania z poczty email, i aby używać szyfrowania end-to-end GPG aby upewnić się, że Twoja korespondencja jest tak chroniona, jak to tylko możliwe.**
