---
title: 'Messagerie: espace de stockage supplémentaire'
bgcolor: '#FFF'
fontcolor: '#7A7A7A'
wider_column: left
---

## Ajoutez de l'espace de stockage à votre boîte aux lettres

Il est possible d'étendre la capacité de stockage de votre messagerie jusqu'à 10 Go pour le coût de 0,15 euro par Go et par mois, à payer annuellement.

Pour demander de l'espace de stockage supplémentaire, vous devez remplir ce  [formulaire](https://disroot.org/fr/forms/extra-storage-mailbox).<br>


---

<br><br>

<a class="button button1" href="https://disroot.org/fr/forms/extra-storage-mailbox">Demander plus d'espace de stockage pour la messagerie</a>
