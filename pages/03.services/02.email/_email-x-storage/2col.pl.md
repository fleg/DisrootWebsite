---
title: 'Extra mailbox Storage'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Zwiększ pojemność swojej skrzynki pocztowej

Można zwiększyć pojemność swojej skrzynki pocztowej z 1GB do 10GB za 0.15 Euro za GB miesięcznie, płacąc rocznie.

Aby uzyskać dodatkową pojemność musisz wypełnić ten [formularz](https://disroot.org/en/forms/extra-storage-mailbox).<br>


---

<br><br>

<a class="button button1" href="https://disroot.org/en/forms/extra-storage-mailbox">Zwiększ pojemność skrzynki pocztowej</a>
