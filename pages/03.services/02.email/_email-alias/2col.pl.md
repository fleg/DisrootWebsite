---
title: 'Email Alias'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
section_id: alias
---

## Aliasy email<br> i podłączanie domen

<a class="button button1" href="https://disroot.org/en/forms/alias-request-form">Poproś o alias</a>
<a class="button button2" href="https://disroot.org/en/forms/domain-linking-form">Podłącz domenę</a>

---

<br>
Alias email jest zwykłym adresem email, który przesyła wszystkie wiadomości pod wskazany adres.

Użytkownicy wspierający mają dostęp do większej ilości aliasów. Poprzez użytkowników wspierających rozumiemy tych, którzy kontrybuują równowartośc co najmniej jednego kubka kawy w miesiący. To nie tak, że promujemy kawę - kawa jest symbolem [wyzysku](http://thesourcefilm.com/) i [nierówności](http://www.foodispower.org/coffee/), więc uznaliśmy, że to dobry sposób by ludzie mogli uznać, ile mogą dać od siebie. Znależliśmy tę [listę cen za kubek kawy na świecie](https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee), może nie jest nadzwyczaj dokładna, ale daje dobry obraz różnicy w cenach. Poświęć chwilę by rozważyć wpłątę. Jeśli możesz "kupić" nam jeden kubek kawy z Rio de Janeiro miesięcznie to OK, ale jeśli stać cię na Podwójne Bezkofeinowe Sojowe Frappucino z Ekstra Śmietanką miesięcznie, to naprawdę możesz pomóc nam utrzymać **Disroot** przy życiu i upewnić się, że będzie dostępne bezpłatnie dla innych, których na to nie stać.

Aby poprosić o alias musisz wypełnić ten [formularz](https://disroot.org/en/forms/alias-request-form).<br>
Jeśli chcesz użyć swojej domeny, wypełnij ten [formularz](https://disroot.org/en/forms/domain-linking-form).

Aby uzyskać więcej informacji na temat tego, jak skonfigurować Twoje nowe aliasy email, skonsultuj się z [naszymi instrukcjami](https://howto.disroot.org/en/tutorials/email/alias).
