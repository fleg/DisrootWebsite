---
title: 'Email Encryption'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: left
wider_column: right
---

## Czym jest szyftowanie

---

Szyfrowanie to zmiana danych za pomocą specjalnego procesu kodowania tak, aby nie można było ich odczytać. Korzystając ze specjalnego procesu dekodowania możesz odzyskać oryginalne dane. Jeśli informacje niezbędne do odszyfrowania danych pozostaną tajne, to nikt inny nie będzie w stanie odczytać oryginalnej wiadomości.

[Wideo - Jak działa szyfrowanie asymetryczne (ang.)](https://www.youtube.com/watch?v=E5FEqGYLL0o)
