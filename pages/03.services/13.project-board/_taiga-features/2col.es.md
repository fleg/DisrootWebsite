---
title: 'Características de Taiga'
wider_column: left
---

## Tablero Kanban

Para quienes que no están familiarizadxs con las herramientas de gestión de proyectos y [scrum](https://es.wikipedia.org/wiki/Scrum_(desarrollo_de_software)), aconsejamos elegir el formato simple Kanban (Para hacer > En curso > Hecho). Puedes leer más sobre Kanban [aquí](https://kanbantool.com/es/metodologia-kanban).


## Proyecto Scrum

Con tareas pendientes, planificación de sprint, puntaje de historia de usuarixs y todas las otras golosinas que todo proyecto Scrum necesita ([Leer más sobre SCRUM](http://www.scrummanager.net/blog/2015/04/libro-gestion-de-proyectos-con-scrum-manager/))


## Tablero de dificultades

El lugar donde puedes recopilar todas las dificultades, sugerencias, problemas, etc, y trabajar colectivamente para resolverlos.


## Wiki

Documenta tu trabajo con una wiki integrada.


## Épicas
Agrupa tareas en [épicas](https://adelatorrefoss.wordpress.com/2014/11/18/que-es-una-historia-epica/) que puedes lograr conforme pasa el tiempo e incluso a través de múltiples proyectos.

---

![](en/taiga-backlog.png?lightbox=1024)

![](en/taiga-kanban.png?lightbox=1024)
