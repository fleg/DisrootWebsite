---
title: Calls
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://calls.disroot.org/">Commencer une conférence</a>

---

![](jitsi_logo.png?resize=80,80)

Le service Disroot Calls est un logiciel de vidéoconférence, alimenté par **Jitsi-Meet**. Il vous permet de réaliser des vidéoconférences et des audioconférences de haute qualité, avec autant de partenaires que vous le souhaitez. Il permet également de diffuser votre bureau ou seulement certaines fenêtres aux autres participants à l'appel.

Disroot Calls : [https://calls.disroot.org/](https://calls.disroot.org/)

Page d'accueil du projet : [https://jitsi.org/jitsi-meet/](https://jitsi.org/jitsi-meet/)

Le code source : [https://github.com/jitsi/jitsi-meet](https://github.com/jitsi/jitsi-meet)
