---
title: Pads
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://pad.disroot.org/">Open a pad</a>
<a class="button button1" href="https://calc.disroot.org/">Create a spreadsheet</a>

---

![](etherpad.png)
## Collaborative editing in really real-time
Disroot's pads are powered by **Etherpad**. A pad is an online text that you can collaboratively edit, in real-time, directly in the web browser. Everybody's changes are instantly reflected on all screens.

Write articles, press releases, to-do lists, etc. together with your friends, fellow students or colleagues.

You don't need any account on Disroot to use this service.

You can use [**Padland**](https://f-droid.org/en/packages/com.mikifus.padland/) on Android to directly open or create your Disroot's pads on your Android device.


Disroot Pads: [https://pad.disroot.org](https://pad.disroot.org)

Project homepage: [http://etherpad.org](http://etherpad.org)

Source code: [https://github.com/ether/etherpad-lite](https://github.com/ether/etherpad-lite)


![](ethercalc.png)
## Collaborative web spreadsheet

Disroot's spreadsheets are powered by **Ethercalc**. You can create an online spreadsheet that you can collaboratively and in real-time edit directly in the web browser. Everybody's changes are instantly reflected on all screens.
Work together on inventories, survey forms, list management, brainstorming sessions and more!

You don't need any account on Disroot to use this service.

Disroot Calc: [https://calc.disroot.org](https://calc.disroot.org)

Project Homepage: [https://ethercalc.net](https://ethercalc.net)

Source code: [https://github.com/audreyt/ethercalc](https://github.com/audreyt/ethercalc)
