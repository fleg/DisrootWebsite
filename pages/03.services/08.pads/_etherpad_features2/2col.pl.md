---
title: 'Ethercalc Features'
text_align: left
wider_column: right
---

![](en/etherpad-history.gif)

---

## Import / export pads

Import and export pads in various formats (plain-text, HTML, Etherpad, Mediawiki).

## Save revisions

Save certain version of your pad.

## Bookmarks

Save a list of bookmarks of your visited pads locally in the browser's local storage.

## Comments

A sidebar to add comments that are linked directly to the text.
