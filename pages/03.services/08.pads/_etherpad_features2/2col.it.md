---
title: 'Caratteristiche di Ethercalc'
text_align: left
wider_column: right
---

![](en/etherpad-history.gif?lightbox=1024)

---

## Importa/esporta pads

Importa ed esporta i pads in vari formati (plain-text, HTML, Etherpad, Mediawiki).

## Salva le varie versioni

Puoi salvare le versioni del tuo pad.

## Segnalibri

Plugin di Ehterpad che permette di salvare in locale una lista dei pad visitati.

## Commenti

Possibilità di caricare dei commenti direttamente collegati al testo.
