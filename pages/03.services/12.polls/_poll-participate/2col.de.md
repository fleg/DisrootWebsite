---
title: 'An Umfrage teilnehmen'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# An einer Umfrage teilnehmen
Wähle Deine Vorlieben, ohne dass Du einen Account erstellen musst. Framadate ermöglicht ausführliche Diskussionen innerhalb der Umfrage, wodurch Du über die bloße Antwortauswahl hinaus an einer Entscheidung teilhaben kannst.

---

![](de/poll_participate.gif)
