---
title: Umfragen
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://poll.disroot.org/">Umfrage erstellen</a>

---

![](framadate_logo.png)

Disroots Umfragen werden bereitgestellt von **Framadate**. Framadate ist ein Onlinedienst für die Planung einer Verabredung oder um das Treffen einer Entscheidung schnell und einfach zu gestalten. Erstelle Deine Umfrage, teile sie mit Deinen Freunden oder Kollegen, so dass sie an der Entscheidung teilhaben können, und erhalte Dein Ergebnis.

Du benötigst keinen Account bei Disroot, um diesen Service zu nutzen.

Disroot-Umfragen: [https://poll.disroot.org](https://poll.disroot.org)

Quellcode: [https://git.framasoft.org/framasoft/framadate](https://git.framasoft.org/framasoft/framadate)
