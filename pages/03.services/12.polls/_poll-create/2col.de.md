---
title: 'Umfrage erstellen'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---
<br>
# Umfrage erstellen

Du kannst eine Umfrage datumsspezifisch oder generell gültig erstellen.

---
<br>
![](de/poll_create.gif)
