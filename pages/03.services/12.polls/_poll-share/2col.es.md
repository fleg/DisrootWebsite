---
title: 'Compartir encuesta'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](poll_share.gif)

---

# Comparte encuestas
Comparte tu encuesta con participantes enviándoles un link.
