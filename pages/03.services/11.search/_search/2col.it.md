---
title: Ricerca
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://search.disroot.org">Vai a Searx'ing</a>
<a class="button button1" href="http://mycroftproject.com/search-engines.html?name=disroot.org">Installa il plugin per il browser</a>

---

![](searx_logo.png)

# Metamotore di ricerca anonimo

Searx è un metamotore, significa che aggrega i risultati di altri motori di ricerca e lo fa senza salvare informazioni sugli utenti.
