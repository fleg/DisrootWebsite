---
title: Disroot to
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
text_align: center
body_classes: modular
---

<br>
## Disroot to platforma dostarczająca usług online oparta o wartości takie jak wolność, prywatność, federacja i decentralizacja.
**Bez śledzenia, bez reklam, bez profilowania i analizy Twoich danych!**
