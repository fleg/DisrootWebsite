---
title: Online-donation
bgcolor: '#fff'
wider_column: left
fontcolor: '#1e5a5e'
---

![](premium.png?class=reward) Disroot opiera się na datkach i wsparciu od swojej społeczności i użytkowników. Jeśli chcesz, aby ten projekt dalej istniał i zapewniał miejsce dla potencjalnych nowych disrooterów, rozważ wsparcie finansowe za pomocą dowolnej z dostępnych metod płatności. [Aliasy i podłączenie domen](/services/email#alias) są dostępne dla wspierających użytkowników.

Możesz nas również wspomóc kupując [dodatkową przestrzeń na emaile](/services/email#storage) i/lub [dodatkową przestrzeń w chmurze](/services/nextcloud#storage).

---

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Przekaż datek za pomocą Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Become a Patron" src="donate/_donate/p_button.png" /></a>

<a href="https://flattr.com/profile/disroot" target=_blank><img alt="Flatter this!" src="donate/_donate/f_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="https://disroot.org/cryptocurrency"><img alt="Kryptowaluty" src="donate/_donate/c_button.png" /></a>

</div>

<span style="color:#4e1e2d; font-size:1.1em; font-weight:bold;">Przelew bankowy:</span>
<span style="color:#4e1e2d; font-size:1.1em;">
Stichting Disroot.org
IBAN NL19 TRIO 0338 7622 05
BIC TRIONL2U
</span>
