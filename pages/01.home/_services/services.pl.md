---
title: Usługi
section_id: services
bgcolor: '#FFF'
bg_img: tent.jpg
fontcolor: '#555'
text_align: left
services:
    -
        title: Email
        icon: email.png
        link: 'https://disroot.org/services/email'
        text: "Bezpłatne i bezpieczne konto email dla Twojego klienta IMAP albo przez przeglądarkę."
        button: 'https://mail.disroot.org'
        buttontext: "Zaloguj się"
    -
        title: Chmura
        icon: cloud.png
        link: 'https://disroot.org/services/nextcloud/'
        text: "Twoje dane pod Twoją kontrolą! Współpracuj, synchronizuj i dziel się plikami, kalendarzami, kontaktami i nie tylko."
        button: 'https://cloud.disroot.org'
        buttontext: "Zaloguj się"
    -
        title: Forum
        icon: forum.png
        link: 'https://disroot.org/services/forum'
        text: "Forum dyskusyjne / lista mailingowa dla Twojej społeczności, grupy lub kolektywu."
        button: 'https://forum.disroot.org'
        buttontext: "Zaloguj się"
    -
        title: Czat XMPP
        icon: xmpp.png
        link: 'https://disroot.org/services/xmpp'
        text: "Zdecentralizowany komunikator internetowy."
        button: 'https://webchat.disroot.org'
        buttontext: "Zaloguj się"
    -
        title: Pads
        icon: pads.png
        link: 'https://disroot.org/services/pads'
        text: "Wspólnie twórz i edytuj dokumenty w przeglądarce, w czasie rzeczywistym."
        button: 'https://pad.disroot.org'
        buttontext: "Utwórz pad"
    -
        title: Calc
        icon: calc.png
        link: 'https://disroot.org/services/pads'
        text: "Wspólnie twórz i edytuj arkusze kalkulacyjne w przeglądarce, w czasie rzeczywistym."
        button: 'https://calc.disroot.org'
        buttontext: "Utwórz arkusz"
    -
        title: 'Paste Bin'
        icon: pastebin.png
        link: 'https://disroot.org/services/privatebin'
        text: "Szyfrowany paste-bin/forum dyskusyjne."
        button: 'https://bin.disroot.org'
        buttontext: "Utwórz wklejkę"
    -
        title: Upload
        icon: upload.png
        link: 'https://disroot.org/services/upload'
        text: "Szyfrowane, krótkoterminowe dzielenie się plikami."
        button: 'https://upload.disroot.org'
        buttontext: "Wyślij plik"
    -
        title: Wyszukiwarka
        icon: search.png
        link: 'https://disroot.org/services/search'
        text: "Anonimowe narzędzie pozwalające korzystać z jednej z wielu wyszukiwarek internetowych."
        button: 'https://search.disroot.org'
        buttontext: "Szukaj"
    -
        title: Ankiety
        icon: polls.png
        link: 'https://disroot.org/services/polls'
        text: "Narzędzie online do szybkiego i łatwego planowania spotkań i podejmowania decyzji."
        button: 'https://poll.disroot.org'
        buttontext: "Utwórz ankietę"
    -
        title: 'Tablica projektowa'
        icon: project_board.png
        link: 'https://disroot.org/services/project-board'
        text: "Narzędzie do zarządzania projektami."
        button: 'https://board.disroot.org'
        buttontext: "Zaloguj się"
    -
        title: 'Wideorozmowy'
        icon: calls.png
        link: 'https://disroot.org/services/calls'
        text: "Narzędzie do wideorozmów."
        button: 'https://calls.disroot.org'
        buttontext: "Nowa rozmowa"
    -
        title: 'Git'
        icon: git.png
        link: 'https://disroot.org/services/git'
        text: "Hosting kodu i wspólna praca nad projektami."
        button: 'https://git.disroot.org'
        buttontext: "Zaloguj się"
    -
        title: 'Audio'
        icon: mumble.png
        link: 'https://disroot.org/services/audio'
        text: "Rozmowy głosowe cechujące się niskim opóźnieniem i wysoką jakością."
        button: 'https://mumble.disroot.org'
        buttontext: "Zaloguj się"
        badge: EXPERIMENTAL
    -
        title: 'Cryptpad'
        icon: cryptpad.png
        link: 'https://disroot.org/services/cryptpad'
        text: "Alternatywa dla popularnych narzędzi biurowych zaprojektowana z myślą o prywatności."
        button: 'https://cryptpad.disroot.org'
        buttontext: "Access"
        badge: EXPERIMENTAL
---
