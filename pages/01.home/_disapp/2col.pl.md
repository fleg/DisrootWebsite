---
title: 'Aplikacja Disroot'
bgcolor: '#1F5C60'
fontcolor: '#fff'
text_align: left
wider_column: right
---

<br>

## Pobierz z [![F-droid](F-Droid.svg.png?resize=80,80&class=imgcenter)](https://f-droid.org/en/packages/org.disroot.disrootapp/) [F-droid](https://f-droid.org/en/packages/org.disroot.disrootapp/?class=lighter)

---

## Aplikacja Disroot
Ta aplikacja jest jak szwajcarski scyzoryk dla platformy Disroot, stworzona przez społeczność dla społeczności. Możesz używać tej aplikacji nawet nie posiadając konta Disroot, aby używać usług nie wymagających logowania - jak etherpad, wysyłanie plików itp.