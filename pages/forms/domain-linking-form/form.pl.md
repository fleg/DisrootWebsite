---
title: 'Domain Linking Form'
header_image: netneutrality.jpg
form:
    name: Domain-linking-form
    fields:
        -
            name: username
            label: 'User Name'
            placeholder: 'Enter your Disroot user name'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: Domain
            label: 'Your domain'
            placeholder: your-domain
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 1'
            label: Alias
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 2'
            label: 'Second Alias (optional)'
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 3'
            label: 'Third Alias (optional)'
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 4'
            label: 'Forth Alias (optional)'
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 5'
            label: 'Fifth Alias (optional)'
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: contribution
            label: 'Contributing via'
            placeholder: select
            type: select
            options:
                patreon: Patreon
                paypal: Paypal
                bank: 'Bank transfer'
                faircoin: Faircoin
                bitcoin: Bitcoin
            validate:
                required: true
        -
            name: amount
            label: Amount
            placeholder: 'EUR/USD/BTC/etc.'
            type: text
            validate:
                pattern: '[A-Za-z0-9., ]*'
                required: true
        -
            name: frequency
            label: Repeated
            type: radio
            default: monthly
            options:
                monthly: Monthly
                yearly: Yearly
            validate:
                required: true
        -
            name: reference
            label: Reference
            type: text
            placeholder: 'Name of payment holder or other transfer reference'
            validate:
                required: true
        -
            name: comments
            type: textarea
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Submit
        -
            type: reset
            value: Reset
    process:
        -
            email:
                from: domain-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] Your domain linking request'
                body: '<br><br>Hi {{ form.value.username|e }}, <br><br><strong>Thank you for your contribution to Disroot.org!</strong><br>We truly appreciate your generosity.<br><br>We will review your request and get back to you as soon as we can.</strong><br><br><hr><br><strong>Here is a summary of the received request:</strong><br><br>{% include ''forms/data.html.twig'' %}'
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Domain linking request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Your domain linking request has been sent!'
        -
            display: thankyou
---

<h1 class="form-title"> Domain Linking Request </h1>
<p class="form-text"><strong> Fill in this form if you would like to use your own domain for Email aliases.</strong>
<br><br>
Please follow these steps to point your domain to our server before you submit the request: <br><strong> (this will also serve to verify the domain is really owned by you and without that we will not proceed to process your request) </strong><br></p>
<h4 class="form-text"> 1. Go to the DNS records settings for your domain</h4>
<p class="form-text"><b>-</b> <i>Point <b>MX record</b> to disroot.org (if you are using your domain currently with other provider, set <b>MX20</b> or higher)</i><br>
<b>-</b> <i>Set TXT record to </i><b>"v=spf1 mx a ptr include:disroot.org -all"</b><br>
</p>
<h4 class="form-text">2. Start donating</h4>
<p class="form-text">The email aliases feature is offered as a <b>"reward"</b> for regular supporters. By regular supporters we think of those who contribute with, at least, the equivalent of one cup of coffee a month. In order for the 'reward' not becoming the only incentive to donate, we expect to see the donation made before we can enable this feature.<br><br>
Check <a href="https://disroot.org/donate">donate page</a> to learn more
</p>
<h4 class="form-text">3. Once we review your request we will contact you to finalize the procedure</h4>
<p class="form-text">
Please take time to consider your contribution. If you can 'buy' us one cup of Rio De Janeiro coffee a month that's OK, but If you can afford a Double Decaf Soy Frappuccino With An Extra Shot And Cream a month, then you can really help us keep the Disroot platform running and make sure it is available for free for other people with less means.
 </p>
