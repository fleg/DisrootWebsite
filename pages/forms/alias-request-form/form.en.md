---
title: 'Alias Request Form'
header_image: introverts.jpg
form:
    name: Alias-request-form
    fields:
        -
            name: username
            label: 'User Name'
            placeholder: 'Enter your Disroot user name'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: 'Alias 1'
            label: Alias
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: true
        -
            name: 'Alias 2'
            label: 'Second Alias (optional)'
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: false
        -
            name: 'Alias 3'
            label: 'Third Alias (optional)'
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: false
        -
            name: 'Alias 4'
            label: 'Forth Alias (optional)'
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: false
        -
            name: 'Alias 5'
            label: 'Fifth Alias (optional)'
            placeholder: your-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.]*'
                required: false
        -
            name: contribution
            label: 'Contributing via'
            placeholder: select
            type: select
            options:
                patreon: Patreon
                paypal: Paypal
                bank: 'Bank transfer'
                faircoin: Faircoin
                bitcoin: Bitcoin
            validate:
                required: true
        -
            name: amount
            label: Amount
            placeholder: 'EUR/USD/BTC/etc.'
            type: text
            validate:
                pattern: '[A-Za-z0-9., ]*'
                required: true
        -
            name: frequency
            label: Repeated
            type: radio
            default: monthly
            options:
                monthly: Monthly
                yearly: Yearly
            validate:
                required: true
        -
            name: reference
            label: Reference
            type: text
            placeholder: 'Name of payment holder or other transfer reference'
            validate:
                required: true
        -
            name: comments
            type: textarea
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Submit
        -
            type: reset
            value: Reset
    process:
        -
            email:
                from: alias-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] Your alias request'
                body: '<br><br>Hi {{ form.value.username|e }}, <br><br><strong>Thank you for your contribution to Disroot.org!</strong><br>We truly appreciate your generosity.<br><br>We will review your alias request and get back to you as soon as we can.</strong><br><br><hr><br><strong>Here is a summary of the received request:</strong><br><br>{% include ''forms/data.html.twig'' %}'
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Alias request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Your alias request has been sent!'
        -
            display: thankyou
---

<h1 class="form-title"> Email Alias Request </h1>
<p class="form-text"><strong>The email aliases feature is offered as a "reward" for regular supporters.</strong> By regular supporters we think of those who contribute with, at least, the equivalent of one cup of coffee a month. In order for the 'reward' not becoming the only incentive to donate, we expect to see the donation made before we can enable this feature.
 <br><br>
It is not that we are promoting coffee, on the contrary - coffee is a symbol for <a href="http://thesourcefilm.com/">exploitation</a> and <a href="http://www.foodispower.org/coffee/">inequality</a> so we thought that it is a good way to let people measure themselves how much they can give.
We found this <a href="https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee">list</a> of coffee cup prices around the world, it might not be very accurate, but it gives a good indication of the different fares.
<br><br>
<strong>Please take time to consider your contribution.</strong> If you can 'buy' us one cup of Rio De Janeiro coffee a month that's OK, but If you can afford a Double Decaf Soy Frappuccino With An Extra Shot And Cream a month, then you can really help us keep the Disroot platform running and make sure it is available for free for other people with less means.</p>
