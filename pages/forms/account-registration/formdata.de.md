---
title: 'Account-Registrierung'
cache_enable: false
process:
    twig: true
---

<h2> Account-Anfrage </h2>

<br>**Vielen Dank für Deine Account-Registrierung bei Disroot. Wie werden Deine Anfrage bearbeiten und uns wieder bei Dir melden. Normalerweise beträgt die Wartezeit nicht länger als 48 Stunden.**
<br>**Um Dir die Wartezeit zu vertreiben kannst Du ja schon mal durch unsere [Tutorials](https://howto.disroot.org/de/) stöbern.**
<br>
**Herzliche Grüße, <br>das Disroot-Team.**
