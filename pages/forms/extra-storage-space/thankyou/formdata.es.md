---
title: 'Almacenamiento extra solicitado'
cache_enable: false
process:
    twig: true
---

<br><br> **Hemos recibido tu solicitud. <br><br>Recibirás una confirmación por correo con tu referencia de facturación. <br> Una vez que hayamos recibido tu pago, te adjudicaremos el espacio adicional. <br><br> ¡Gracias por apoyar a Disroot!**
<br>
<hr>
<br>
**Aquí está el resumen de la solicitud recibida:**
