---
title: Kontakt
bgcolor: '#1F5C60'
fontcolor: '#FFF'
process:
    markdown: true
    twig: true
twig_first: false
wider_column: right
---

## Jak się z nami skontaktować?

---

Jeśli chcesz wysłać nam uwagę, masz pytanie, chcesz porozmawiać albo po prostu ponarzekać na niski poziom usług i wsparcia:
